%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initializes Paths for M_MAPLIB within some project
%
% Description:
%  This script finds location of M_MAPLIB root and adds the required paths
%  to the main Matlab paths.
%
% Modifications:
%  MA-20Jun16 initial draft
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MAPLIBrootdir = fileparts( mfilename('fullpath'));


%% Add paths to required.
addpath(fullfile(MAPLIBrootdir, 'm_map'))

clear MAPLIBrootdir